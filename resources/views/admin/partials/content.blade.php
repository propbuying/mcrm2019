
					<!-- begin:: Content -->
					<div class="k-content	k-grid__item k-grid__item--fluid k-grid k-grid--hor" id="k_content">

						<!-- begin:: Content Head -->
						<div class="k-content__head	k-grid__item">
							<div class="k-content__head-main">
								<h3 class="k-content__head-title">Dashboard</h3>
								<div class="k-content__head-breadcrumbs">
									<a href="#" class="k-content__head-breadcrumb-home"><i class="flaticon2-shelter"></i></a>
									<span class="k-content__head-breadcrumb-separator"></span>
									<a href="" class="k-content__head-breadcrumb-link">Dashboards</a>
									<span class="k-content__head-breadcrumb-separator"></span>
									<a href="" class="k-content__head-breadcrumb-link">Navy Aside</a>

									<!-- <span class="k-content__head-breadcrumb-link k-content__head-breadcrumb-link--active">Active link</span> -->
								</div>
							</div>
							<div class="k-content__head-toolbar">
								<div class="k-content__head-wrapper">
									<div class="dropdown dropdown-inline k-hide" data-toggle="k-tooltip" title="" data-placement="left" data-original-title="Quick actions">
										<button type="button" class="btn btn-sm btn-elevate btn-danger btn-bold btn-upper dropdown-toggle-" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
											New
										</button>
										<div class="dropdown-menu dropdown-menu-right">
											<a class="dropdown-item" href="#"><i class="la la-plus"></i> New Report</a>
											<a class="dropdown-item" href="#"><i class="la la-user"></i> Add Customer</a>
											<a class="dropdown-item" href="#"><i class="la la-cloud-download"></i> New Download</a>
											<div class="dropdown-divider"></div>
											<a class="dropdown-item" href="#"><i class="la la-cog"></i> Settings</a>
										</div>
									</div>
									<a href="#" class="btn btn-sm btn-elevate btn-brand" id="k_dashboard_daterangepicker" data-toggle="k-tooltip" title="" data-placement="left" data-original-title="Select dashboard daterange">
										<span class="k-opacity-7" id="k_dashboard_daterangepicker_title">Today:</span>&nbsp;
										<span class="k-font-bold" id="k_dashboard_daterangepicker_date">Apr 4</span>
										<i class="flaticon-calendar-with-a-clock-time-tools k-padding-l-5 k-padding-r-0"></i>
									</a>
								</div>
							</div>
						</div>

						<!-- end:: Content Head -->

						<!-- begin:: Content Body -->
						<div class="k-content__body	k-grid__item k-grid__item--fluid" id="k_content_body">

							<!--begin::Dashboard 1-->

							<!--begin::Row-->
							<div class="row">
								<div class="col-lg-12 col-xl-4 order-lg-1 order-xl-1">

									<!--begin::Portlet-->
									 
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2>Users Management</h2>
        </div>
        <div class="pull-right">
            <a class="btn btn-info" data-toggle="modal" data-target="#create_user" style="color:#fff;"> Create New User</a>
        </div>
    </div>
</div>


@if ($message = Session::get('success'))
<div class="alert alert-success">
  <p>{{ $message }}</p>
</div>
@endif


<table class="table table-bordered">
 <tr>
   <th>No</th>
   <th>Name</th>
   <th>Email</th>
   <th>Roles</th>
   <th width="280px">Action</th>
 </tr>
 @foreach ($data as $key => $user)
  <tr>
    <td>{{ ++$i }}</td>
    <td>{{ $user->name }}</td>
    <td>{{ $user->email }}</td>
    <td>
      @if(!empty($user->getRoleNames()))
        @foreach($user->getRoleNames() as $v)
           <label class="badge badge-success">{{ $v }}</label>
        @endforeach
      @endif
    </td>
    <td>
       <a class="btn btn-info show_user" id="$user->id&&$user->name&&$user->email&&" href="{{ route('users.show',$user->id) }}">Show</a>
       <a class="btn btn-primary" href="{{ route('users.edit',$user->id) }}">Edit</a>
 
    </td>
  </tr>
 @endforeach
</table>


{!! $data->render() !!}

 <div class="k-section__content k-section__content--border">

													 
													<!-- Modal -->
													<div class="modal fade" id="create_user" tabindex="-1" role="dialog" aria-labelledby="create_user_modal" aria-hidden="true">
														<div class="modal-dialog" role="document">
															<div class="modal-content">
																<div class="modal-header">
																	<h5 class="new-user modal-title" id="create_user_modal">Create New User</h5>
																	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																		<span aria-hidden="true">&times;</span>
																	</button>
																</div>
																<div class="modal-body">
																	{!! Form::open(array('route' => 'users.store','method'=>'POST')) !!}
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Name:</strong>
            {!! Form::text('name', null, array('placeholder' => 'Name','class' => 'form-control')) !!}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">    
            <strong>Email:</strong>
            {!! Form::text('email', null, array('placeholder' => 'Email','class' => 'form-control')) !!}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Password:</strong>
            {!! Form::password('password', array('placeholder' => 'Password','class' => 'form-control')) !!}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Confirm Password:</strong>
            {!! Form::password('confirm-password', array('placeholder' => 'Confirm Password','class' => 'form-control')) !!}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Role:</strong>
            {!! Form::select('roles[]', $roles,[], array('class' => 'form-control','multiple')) !!}
        </div>
    </div>
     
</div>
{!! Form::close() !!}
																</div>
																<div class="modal-footer">
																	<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
																	 <button type="submit" class="btn btn-brand">Save changes</button>
																</div>
															</div>
														</div>
													</div>
												</div>
										 
							</div>

							<!--end::Row-->

							 

							<!--end::Dashboard 1-->
						</div>

						<!-- end:: Content Body -->
					</div>

					<!-- end:: Content -->
