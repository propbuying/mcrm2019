<head>
		<meta charset="utf-8">
		<title>Keen | Dashboard</title>
		<meta name="description" content="Latest updates and statistic charts">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">

		<!--begin::Web font -->
		<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
		<script>
			WebFont.load({
                google: {"families":["Poppins:300,400,500,600,700"]},
                active: function() {
                    sessionStorage.fonts = true;
                }
            });
        </script>
 
		<!--end::Web font -->

		<!--begin::Page Vendors Styles -->
		<link href="../public/assets/vendors/custom/fullcalendar/fullcalendar.bundle.css" rel="stylesheet" type="text/css">

		<!--end::Page Vendors Styles -->

		<!--begin:: Global Mandatory Vendors -->
		<link href="../public/assets/vendors/general/perfect-scrollbar/css/perfect-scrollbar.css" rel="stylesheet" type="text/css">

		<!--end:: Global Mandatory Vendors -->

		<!--begin:: Global Optional Vendors -->
		<link href="../public/assets/vendors/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
		<link href="../public/assets/vendors/general/tether/dist/css/tether.css" rel="stylesheet" type="text/css">
		<link href="../public/assets/vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css" rel="stylesheet" type="text/css">
		<link href="../public/assets/vendors/general/bootstrap-datetime-picker/css/bootstrap-datetimepicker.css" rel="stylesheet" type="text/css">
		<link href="../public/assets/vendors/general/bootstrap-timepicker/css/bootstrap-timepicker.css" rel="stylesheet" type="text/css">
		<link href="../public/assets/vendors/general/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet" type="text/css">
		<link href="../public/assets/vendors/general/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.css" rel="stylesheet" type="text/css">
		<link href="../public/assets/vendors/general/bootstrap-select/dist/css/bootstrap-select.css" rel="stylesheet" type="text/css">
		<link href="../public/assets/vendors/general/nouislider/distribute/nouislider.css" rel="stylesheet" type="text/css">
		<link href="../public/assets/vendors/general/owl.carousel/dist/assets/owl.carousel.css" rel="stylesheet" type="text/css">
		<link href="../public/assets/vendors/general/owl.carousel/dist/assets/owl.theme.default.css" rel="stylesheet" type="text/css">
		<link href="../public/assets/vendors/general/dropzone/dist/dropzone.css" rel="stylesheet" type="text/css">
		<link href="../public/assets/vendors/general/summernote/dist/summernote.css" rel="stylesheet" type="text/css">
		<link href="../public/assets/vendors/general/bootstrap-markdown/css/bootstrap-markdown.min.css" rel="stylesheet" type="text/css">
		<link href="../public/assets/vendors/general/animate.css/animate.css" rel="stylesheet" type="text/css">
		<link href="../public/assets/vendors/general/toastr/build/toastr.css" rel="stylesheet" type="text/css">
		<link href="../public/assets/vendors/general/morris.js/morris.css" rel="stylesheet" type="text/css">
		<link href="../public/assets/vendors/general/sweetalert2/dist/sweetalert2.css" rel="stylesheet" type="text/css">
		<link href="../public/assets/vendors/general/socicon/css/socicon.css" rel="stylesheet" type="text/css">
		<link href="../public/assets/vendors/custom/vendors/line-awesome/css/line-awesome.css" rel="stylesheet" type="text/css">
		<link href="../public/assets/vendors/custom/vendors/flaticon/flaticon.css" rel="stylesheet" type="text/css">
		<link href="../public/assets/vendors/custom/vendors/flaticon2/flaticon.css" rel="stylesheet" type="text/css">
		<link href="../public/assets/vendors/custom/vendors/fontawesome5/css/all.min.css" rel="stylesheet" type="text/css">

		<!--end:: Global Optional Vendors -->

		<!--begin::Global Theme Styles -->
		<link href="../public/assets/demo/default/base/style.bundle.css" rel="stylesheet" type="text/css">

		<!--end::Global Theme Styles -->

		<!--begin::Layout Skins -->
		<link href="../public/assets/demo/default/skins/header/base/light.css" rel="stylesheet" type="text/css">
		<link href="../public/assets/demo/default/skins/header/menu/light.css" rel="stylesheet" type="text/css">
		<link href="../public/assets/demo/default/skins/brand/navy.css" rel="stylesheet" type="text/css">
		<link href="../public/assets/demo/default/skins/aside/navy.css" rel="stylesheet" type="text/css">

		<!--end::Layout Skins -->
		<link rel="shortcut icon" href="../public/assets/media/logos/favicon.ico">
	<style type="text/css">span.im-caret {
    -webkit-animation: 1s blink step-end infinite;
    animation: 1s blink step-end infinite;
}

@keyframes blink {
    from, to {
        border-right-color: black;
    }
    50% {
        border-right-color: transparent;
    }
}

@-webkit-keyframes blink {
    from, to {
        border-right-color: black;
    }
    50% {
        border-right-color: transparent;
    }
}

span.im-static {
    color: grey;
}

div.im-colormask {
    display: inline-block;
    border-style: inset;
    border-width: 2px;
    -webkit-appearance: textfield;
    -moz-appearance: textfield;
    appearance: textfield;
}

div.im-colormask > input {
    position: absolute;
    display: inline-block;
    background-color: transparent;
    color: transparent;
    -webkit-appearance: caret;
    -moz-appearance: caret;
    appearance: caret;
    border-style: none;
    left: 0; /*calculated*/
}

div.im-colormask > input:focus {
    outline: none;
}

div.im-colormask > input::-moz-selection{
    background: none;
}

div.im-colormask > input::selection{
    background: none;
}
div.im-colormask > input::-moz-selection{
    background: none;
}

div.im-colormask > div {
    color: black;
    display: inline-block;
    width: 100px; /*calculated*/
}</style><style type="text/css">/* Chart.js */
@-webkit-keyframes chartjs-render-animation{from{opacity:0.99}to{opacity:1}}@keyframes chartjs-render-animation{from{opacity:0.99}to{opacity:1}}.chartjs-render-monitor{-webkit-animation:chartjs-render-animation 0.001s;animation:chartjs-render-animation 0.001s;}</style></head>
