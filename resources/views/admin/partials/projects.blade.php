<html>
@include('admin.partials.head')
	<!-- end::Head -->
<body class="k-header--fixed k-header-mobile--fixed k-aside--enabled k-aside--fixed">

		<!-- begin:: Page -->

		<!-- begin:: Header Mobile -->
		<div id="k_header_mobile" class="k-header-mobile  k-header-mobile--fixed ">
			<div class="k-header-mobile__logo">
				<a href="index.html">
					<img alt="Logo" src="../public/assets/media/logos/logo-6.png">
				</a>
			</div>
			<div class="k-header-mobile__toolbar">
				<button class="k-header-mobile__toolbar-toggler k-header-mobile__toolbar-toggler--left" id="k_aside_mobile_toggler"><span></span></button>
				<button class="k-header-mobile__toolbar-toggler" id="k_header_mobile_toggler"><span></span></button>
				<button class="k-header-mobile__toolbar-topbar-toggler" id="k_header_mobile_topbar_toggler"><i class="flaticon-more"></i></button>
			</div>
		</div>

		<!-- end:: Header Mobile -->
		<div class="k-grid k-grid--hor k-grid--root">
			<div class="k-grid__item k-grid__item--fluid k-grid k-grid--ver k-page">

				<!-- begin:: Aside -->
				<button class="k-aside-close " id="k_aside_close_btn"><i class="la la-close"></i></button>
			 @include('admin.partials.sidebar')
			 
			 @include('admin.partials.header1')
			 
				<!-- end:: Aside -->
				<div class="k-grid__item k-grid__item--fluid k-grid k-grid--hor k-wrapper textfeild" id="k_wrapper" style="padding-left: 279px;">
 <!--begin::Portlet-->
									<div class="k-portlet">
										<div class="k-portlet__head">
											<div class="k-portlet__head-label">
												<h3 class="k-portlet__head-title">Upload Project</h3>
											</div>
										</div>

										<!--begin::Form-->
										<div class="k-portlet__body">
                                                                        <form class="k-form k-form--label-right" method="post" id="uploadProjectForm123" >
																				<input type='hidden' name="_token" id="token" value='{{csrf_token()}}'>
											
											
												<div class="form-group row">
													<div class="col-lg-6">
														<label>Full Name:</label>
                                                                                                                <input type="text" name="full_name" class="form-control" placeholder="Enter full name">
														 </div>
                                                                                                    <div class="col-lg-6">
														<label>Phase:</label>
                                                                                                                <select name="phase" class="form-control k_selectpicker" data-live-search="true">
                                                                                                                    <option data-tokens="ketchup mustard">Select Phase</option>
                                                                                                                    @for ($i=0;$i<19;$i++)
                                                                                                                <option data-tokens="ketchup mustard">{{$i}}</option>
                                                                                                                @endfor
												</select>
                                                                                                    </div>
                                                                                                </div>
                                                                                            <div class="form-group row">
                                                                                                    <div class="col-lg-12">
														<label>Address</label>
                                                                                                                <input type="text" name="address" class="form-control" placeholder="Enter Address">
														 </div>
                                                                                            </div>
                                                                                            <div class="form-group row">
													<div class="col-lg-4">
														<label>Building:</label>
                                                                                                                <select name="building" class="form-control k_selectpicker" data-live-search="true">
													<option data-tokens="ketchup mustard">Select Building</option>
                                                                                                                    @for ($i=0;$i<50;$i++)
                                                                                                                <option data-tokens="ketchup mustard">{{$i}}</option>
                                                                                                                @endfor
												</select>
                                                                                                    </div>
													<div class="col-lg-4">
														<label>Total Area:</label>
                                                                                                                <select name="area" class="form-control k_selectpicker" data-live-search="true">
													<option data-tokens="ketchup mustard">Select Area</option>
                                                                                                                    @for ($i=0;$i<19;$i++)
                                                                                                                <option data-tokens="ketchup mustard">{{$i}}</option>
                                                                                                                @endfor
												</select>
                                                                                                    </div>
                                                                                                    <div class="col-lg-4">
														<label>Status:</label>
                                                                                                                <select name="status" class="form-control k_selectpicker" data-live-search="true">
														<option data-tokens="ketchup mustard">Select Phase</option>
                                                                                                                    @for ($i=0;$i<4;$i++)
                                                                                                                <option data-tokens="ketchup mustard">{{$i}}</option>
                                                                                                                @endfor
                                                                                                                </select>
												</select>
                                                                                                    </div>
                                                                                            </div>
                                                                                            <div class="form-group row">
                                                                                                     <div class="col-lg-2">
														<label>Launch Date:</label>
                                                                                                                <select name="launch_month" class="form-control k_selectpicker" data-live-search="true">
													<option data-tokens="ketchup mustard">Select Month</option>
                                                                                                                   @for($m=1; $m<=12; ++$m){
                                                                                                                   <option> {{date('F', mktime(0, 0, 0, $m, 1))}}</option>;
                                                                                                                   @endfor
												</select>
                                                                                                    </div>
                                                                                                    <div class="col-lg-2">
														<label>&nbsp;</label>
														<select name="launch_year" class="form-control k_selectpicker" data-live-search="true">
													 @for($m=2019; $m>=2000; --$m){
                                                                                                                   <option> {{$m}}</option>
                                                                                                                   @endfor
												</select>
                                                                                                    </div>
                                                                                                    <div class="col-lg-2">
                                                                                                    </div>
                                                                                                    
                                                                                                    <div class="col-lg-2">
														<label>Professional Date:</label>
														<select name="prof_month" class="form-control k_selectpicker" data-live-search="true">
                                                                                                                    <option data-tokens="ketchup mustard">Select Month</option>
                                                                                                                 @for($m=1; $m<=12; ++$m){
                                                                                                                   <option> {{date('F', mktime(0, 0, 0, $m, 1))}}</option>;
                                                                                                                   @endfor
												</select>
                                                                                                    </div>
                                                                                                    <div class="col-lg-2">
														<label>&nbsp;</label>
														<select   name="prof_year" class="form-control k_selectpicker" data-live-search="true">
													 @for($m=2019; $m>=2000; --$m){
                                                                                                                   <option> {{$m}}</option>
                                                                                                                   @endfor
												</select>
												
                                                                                                    </div>
												</div>
												
																									<div class="col-lg-12">
														<div class="k-repeater">
															<div data-repeater-list="demo1">
																<div data-repeater-item="" class="k-repeater__item">
																	<div class="form-group row">
                                                                                                     <div class="col-lg-4">														 
                                                                                            <select class="form-control k_selectpicker" data-live-search="true">
													<option data-tokens="ketchup mustard">BHK</option>
													<option data-tokens="mustard">1 BHK</option>
													<option data-tokens="frosting">2 BHK</option>
                                                                                                        <option data-tokens="frosting">3 BHK</option>
                                                                                                        <option data-tokens="frosting">4 BHK</option>
												</select>
                                                                                                    </div>
																									<div class="col-lg-4">
                                                                                                            <input type="text" name="contact" class="form-control" placeholder="Enter contact number">
														 </div>
													<div class="col-lg-4">
														 <div class="k-input-icon k-input-icon--right">
                                                                                                                     <input type="text" name="price" class="form-control" placeholder="Price">
															<span class="k-input-icon__icon k-input-icon__icon--right"><span><i class="la la-info-circle"></i></span></span>
														</div>
														 </div>
																	<div class="k-separator k-separator--space-sm"></div>
																</div>
															</div>
															<div class="k-repeater__add-data">
																<span data-repeater-create="" class="btn btn-info btn-sm">
																	<i class="la la-plus"></i> Add
																</span>
															</div>
														</div>
													</div>
													
                                                                                                </div>
                                                                                            <div class="form-group row">
                                                                                                   <div class="col-lg-12">
                                                                                                        <label>Upload Project</label>
                                                                                                        <div></div>
                                                                                                        <div class="custom-file">
                                                                                                            <input type="file" class="custom-file-input" name="uploadProject" id="upload_project">
                                                                                                            <label class="custom-file-label" for="customFile">Choose file</label>
                                                                                                        </div>
                                                                                                    </div>
													 
												</div>
                                                                                                                                                                <div class="form-group row">
                                                                                                                                                                   <div class="col-lg-4"></div>
														<div class="col-lg-8">
															<input type="submit" class="btn btn-primary">
															<button type="reset" class="btn btn-secondary">Cancel</button>
														</div> 
                                                                                                                                                                </div>
												 
											
											
										</form>
										
 <div class="k-portlet__foot">
												<div class="k-form__actions">
													<div class="row">
														
													</div>
												</div
										 <div class="form-group row">
                                                                                                   <div class="col-lg-12">
                                                                                                      
<table id="example" class="display" style="width:100%">
        <thead>
            <tr>
                <th>First name</th>
                <th>Last name</th>
                <th>Position</th>
                <th>Office</th>
                <th>Start date</th>
                <th>Salary</th>
                
            </tr>
        </thead>
         
    </table>
			   </div>
										<!--end::Form-->
										</div>
									</div>
 
											</div>
</div>
  
				</div>

			</div>
		</div>
@include('admin.partials.footer')

<script type="text/javascript"> 
   $(document).ready(function() {
       var table = $('#example').DataTable( {
        "processing": true,
        "serverSide": true,
        "ajax": "./getData"
    } );
 
    $('#uploadProjectForm123').on('submit', function(e) {
		
       alert("dddd");           
    e.preventDefault(); 
    $.ajax({
        type: "POST",
        url: './store-project',
        data: $(this).serialize(),
        success: function(msg) {
        alert(msg);
         table.ajax.reload();
		$('#uploadProjectForm').trigger("reset");
        }
    });
});
} );
</script>
<script src="../public/assets/demo/default/custom/components/forms/layouts/repeater.js" type="text/javascript"></script>
		<!-- end:: Page -->
                
                