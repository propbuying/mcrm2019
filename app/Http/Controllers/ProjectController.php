<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Model\ProjectModel;
class ProjectController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
 public function storeProject(Request $request)
    {
		ini_set('memory_limit', '148000M');
        $input = $request->all();
        
	    $full_name = $request->input('full_name');
		$phase = $request->input('phase');
		$sddress = $request->input('address');
		$status = $request->input('status');
		$launch_month = $request->input('launch_month');
		$launch_year = $request->input('launch_year');
		$prof_month = $request->input('prof_month');
		$prof_year = $request->input('prof_year');
		$area = $request->input('area');
		$building = $request->input('building'); 
		$data=array('project_name'=>$full_name,"phase"=>$phase,"launch_date"=>$launch_month.$launch_year,"prof_date"=>$prof_month.$prof_year,"building"=>$building,"sddress"=>$sddress,"area"=>$area,"status"=>$status);
		$result=DB::table('projects')->insertGetId($data);
                foreach($input['demo1'] as $dataMultObj){
                $dataMulArr=array();    
                $dataMulArr['contact']=$dataMultObj['contact'];
                $dataMulArr['price']=$dataMultObj['price'];
                $dataMulArr['project_id']=$result;
               $resultMul=DB::table('project_detail')->insert($dataMulArr);
                }
		echo "Your data success Fully Stored";
	  
  
    }
	public function getProjects()
    { 
        $projectObj= DB::table('projects')->get();
		 return view('admin/partials/projects',compact('projectObj'));
	    
    }
    public function getData()
    {
        $jsonObj=array();
        $jsonObj['draw']='2';
         $jsonObj['recordsTotal']='200';
          $jsonObj['recordsFiltered']='200';
           $jsonObj['data']=array();
         $projectObj= DB::table('projects')->get();
		 foreach($projectObj as $prObj)
		 {
                     $tempArr=array($prObj->project_name,$prObj->project_name,$prObj->project_name,$prObj->project_name,$prObj->project_name,$prObj->project_name);
			  //  $jsonObj['data'].='["'.$prObj->project_name.'","'.$prObj->project_name.'","'.$prObj->project_name.'","'.$prObj->project_name.'","'.$prObj->project_name.'","'.$prObj->project_name.'"],';
			array_push($jsonObj['data'],$tempArr); 
		 }
		// $jsonObj['data'].=']'; 
      echo json_encode($jsonObj);
       '{
  "draw": 2,
  "recordsTotal": 57,
  "recordsFiltered": 57,
  "data": [
    [
      "Charde",
      "Marshall",
      "Regional Director",
      "San Francisco",
      "16th Oct 08",
      "$470,600"
    ],
    [
      "Colleen",
      "Hurst",
      "Javascript Developer",
      "San Francisco",
      "15th Sep 09",
      "$205,500"
    ],
    [
      "Dai",
      "Rios",
      "Personnel Lead",
      "Edinburgh",
      "26th Sep 12",
      "$217,500"
    ],
    [
      "Donna",
      "Snider",
      "Customer Support",
      "New York",
      "25th Jan 11",
      "$112,000"
    ],
    [
      "Doris",
      "Wilder",
      "Sales Assistant",
      "Sidney",
      "20th Sep 10",
      "$85,600"
    ],
    [
      "Finn",
      "Camacho",
      "Support Engineer",
      "San Francisco",
      "7th Jul 09",
      "$87,500"
    ],
    [
      "Fiona",
      "Green",
      "Chief Operating Officer (COO)",
      "San Francisco",
      "11th Mar 10",
      "$850,000"
    ],
    [
      "Garrett",
      "Winters",
      "Accountant",
      "Tokyo",
      "25th Jul 11",
      "$170,750"
    ],
    [
      "Gavin",
      "Joyce",
      "Developer",
      "Edinburgh",
      "22nd Dec 10",
      "$92,575"
    ],
    [
      "Gavin",
      "Cortez",
      "Team Leader",
      "San Francisco",
      "26th Oct 08",
      "$235,500"
    ]
  ]
}';
    }
}
