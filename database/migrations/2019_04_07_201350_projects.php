<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Projects extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::create('projects', function (Blueprint $table) {
			$table->bigIncrements('project_id');
            $table->string('project_name');
            $table->integer('phase');
             $table->text('sddress');
            $table->string('building');
			 $table->string('area');
			$table->integer('status');
			$table->string('launch_date');
			$table->string('prof_date');
			$table->string('project_doc');
            $table->rememberToken();
            $table->timestamps();
		});
	}
    

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
